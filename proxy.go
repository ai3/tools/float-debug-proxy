package main

import (
	"context"
	"errors"
	"flag"
	"log"
	"net"

	"github.com/armon/go-socks5"
	"github.com/miekg/dns"
)

var (
	addr       = flag.String("addr", ":9051", "`addr`ess to listen on")
	resolverIP = flag.String("resolver", "127.0.0.1", "IP `addr`ess of the DNS resolver to use")
)

// IPv4-only resolver (for now).
type resolver struct {
	addr string
}

func (r *resolver) Resolve(ctx context.Context, name string) (context.Context, net.IP, error) {
	m := new(dns.Msg)
	m.SetQuestion(dns.Fqdn(name), dns.TypeA)
	m.RecursionDesired = true

	in, err := dns.Exchange(m, r.addr)
	if err != nil {
		return ctx, nil, err
	}
	for _, rec := range in.Answer {
		if a, ok := rec.(*dns.A); ok {
			return ctx, a.A, nil
		}
	}
	return ctx, nil, errors.New("not found")
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	config := socks5.Config{
		Resolver: &resolver{*resolverIP + ":53"},
	}
	server, err := socks5.New(&config)
	if err != nil {
		log.Fatal(err)
	}

	if err := server.ListenAndServe("tcp", *addr); err != nil {
		log.Fatal(err)
	}
}
